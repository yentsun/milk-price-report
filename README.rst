=================
Milk Price Report
=================

This package is a set of data models, validation schemas and utilities for
keeping track of consumer goods prices. It also includes data models for
products, merchants, manufacturers.