from setuptools import setup

setup(
    name = 'MilkPriceReport',
    version = '0.4',
    author = 'Maxim Korinets',
    author_email = 'mkorinets@gmail.com',
    packages = ['milkpricereport'],
    url = 'https://bitbucket.org/yentsun/milk-price-report',
    license = 'LICENSE.txt',
    description = 'A set of data models for keeping track of consumer prices',
    long_description = open('README.rst').read(),
    install_requires = ['colander',
                        'pyyaml',
                        'numpy',
                        'zodb'],
)